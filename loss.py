import torch
from torch.nn import functional as F

class FocalLoss(torch.nn.Module):
    """
    Focal Loss. Loss metric first described in
    https://arxiv.org/pdf/1708.02002.pdf . Since there is a strong
    imbalance in the class labels, using standard cross entropy here
    might hinder the learning process. Focal Loss is a loss metric
    designed to prevent the large number of negative examples from
    overwhelming the small positive set.

    Arguments:
        gamma (float): hyperparameter that is called the focusing
            parameter in the paper. Default is 2.0 beacuse of the
            results in table 1.
    """

    def __init__(self, gamma=2):
        super().__init__()
        self.gamma = gamma

    def forward(self, pred, target):
        max_val = (-pred).clamp(min=0)
        loss = pred - pred*target + max_val +\
            ((-max_val).exp() + (-pred - max_val).exp()).log()
        invprobs = F.logsigmoid(-pred * (target * 2.0 - 1.0))
        loss *= (invprobs * self.gamma).exp()
        return loss.sum(dim=1).mean()
