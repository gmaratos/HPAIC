import os
import torch
import numpy as np
import pandas as pd
import skimage as si
from skimage.transform import resize
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset
from torchvision.transforms import Normalize

class AtlasDataset(Dataset):
    """
    AtlasDataset. torch Dataset object that defines how a single
    example is processed from its raw state into something that
    can be passed to a network for inference

    Arguments:
        dpath (string): path to the root folder that contains the
            images and train.csv
        mode (string): can be 'train' or 'valid' and will specify
            which set of indicies will be used
    """

    def __len__(self):
        return len(self.inds)

    def __init__(self, dpath, mode):
        self.dcsv = pd.read_csv(os.path.join(dpath, "train.csv"))
        self.dpath = dpath
        self.normalize = Normalize([0.0764, 0.0519, 0.0502, 0.0770],\
            [0.0197, 0.0214, 0.0110, 0.0194])
        np.random.seed(0)
        inds = np.arange(len(self.dcsv))
        np.random.shuffle(inds)
        splt = int(len(self.dcsv)*0.1)
        if mode == 'train':
            self.inds = inds[:splt*8]
        if mode == 'val':
            self.inds = inds[splt*8:splt*8+splt]
        if mode == 'test':
            self.inds = inds[splt*9:]
        self.mode = mode

    def __getitem__(self, indx):
        ipref = self.dcsv.iloc[self.inds[indx], 0]
        lstr = self.dcsv.iloc[self.inds[indx], 1]
        ipref = os.path.join(self.dpath, ipref)
        inp = np.array([\
            si.io.imread(os.path.join(ipref+'_red.png')),
            si.io.imread(os.path.join(ipref+'_blue.png')),
            si.io.imread(os.path.join(ipref+'_green.png')),
            si.io.imread(os.path.join(ipref+'_yellow.png'))],
            dtype=np.float)
        inp /= 255
        #temporary solution:need to make new image size a parameter
        inp = resize(inp, (4, 256, 256), anti_aliasing=False)
        lind = [int(x) for x in lstr.split()]
        label = np.zeros(28)
        label[lind] = 1
        inp = torch.tensor(inp)
        inp = self.normalize(inp)
        label = torch.tensor(label, dtype=torch.float)
        return (inp.float(), label)
