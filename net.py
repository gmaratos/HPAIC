import torch
from torch import nn
from torchvision.models.resnet import ResNet, BasicBlock, model_urls
import torch.utils.model_zoo as model_zoo

class AdaptiveConcatPool2d(nn.Module):
    """
    AdaptiveConcatPool2d. Pooling layer that computes both AvgPooling
    and MaxPooling, then returns the concatenation. This improves
    performance beyond using one or the other

    Arguments:
        sz (int): the size of the pooling field for both avg and max
    """

    def __init__(self, sz=1):
        super().__init__()
        self.ap = nn.AdaptiveAvgPool2d(sz)
        self.mp = nn.AdaptiveMaxPool2d(sz)

    def forward(self, x):
        return torch.cat([self.mp(x), self.ap(x)], 1)

class Resnet34_mod(ResNet):
    """
    Resnet34_mod. Class that defines a Resnet34 architecture that
    uses weights from a pretrained model (Imagenet), with some slight
    modifications for the HPAIC task
    """

    def __init__(self):
        super().__init__(BasicBlock, [3, 4, 6, 3])
        self.load_state_dict(model_zoo.load_url(model_urls['resnet34']))
        init_layer = torch.nn.Parameter(torch.cat((self.conv1.weight,
            self.conv1.weight[:, :1, :, :]), dim=1))
        self.conv1 = nn.Conv2d(4, 64, kernel_size=7, stride=2, padding=3,
            bias=False)
        self.conv1.weight = init_layer
        self.cpool = AdaptiveConcatPool2d()
        self.bn2 = nn.BatchNorm1d(1024)
        self.d1 = nn.Dropout()
        self.fc1 = nn.Linear(1024, 512)
        self.rl = nn.ReLU()
        self.bn3 = nn.BatchNorm1d(512)
        self.d2 = nn.Dropout()
        self.fc2 = nn.Linear(512, 28)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.cpool(x)
        x = x.view(x.size(0), -1)
        x = self.bn2(x)
        x = self.d1(x)
        x = self.fc1(x)
        x = self.rl(x)
        x = self.bn3(x)
        x = self.d2(x)
        x = self.fc2(x)
        return x
