\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{./assignment}

\lecturenumber{1}
\duedate{23:59, December 4, 2018}
\stuinfo{George Maratos, Michael Avendano}
    {gmarat2@uic.edu, mavend2@uic.edu}

\begin{document}
\maketitle

\section*{Problem Description}
Researchers frequently use high content high-throughput microscopy when
studying protein function, and such techniques benefit from large
scale automation. Large scale automation of this kind will produce large
amounts of unlabeled data about temporal and spatial distributions
of the fluorescent marks which are used to tag certain proteins [1].
Methods used in the Data Sciences can aid researchers in interpreting
these large scale data sets, and developing a better understanding about
human cells and disease.

One task within this domain involves the classification of mixed patterns
of proteins. Researchers tag groups of cells with fluorescent molecules,
and observe the molecular distribution under a microscope. These
molecules will attach themselves to specific organelles inside of a cell.
A competition on Kaggle [2], has a dataset of this nature, with markers
on proteins of interest, the nucleus, microtubules, and the endoplasmic
reticulum. Figure 1 has examples of such images.

The task is to identify which organelles contain the protein of interest.
The targets being labels identifying the organelle.
The task is a multi label classification problem, where given an example
image, predict which classes it belongs too. An example can have more
than one class, so the space of possible labels is $2^{28}$.
Images are difficult to work with in a traditional setting, because they
are high dimensional and finding features to reduce complexity is not
a simple task. Our approach is to extract useful information through various
ways of clustering then to use methods in deep learning which
have shown high success in computer vision tasks. We will apply a standard
architecture[s????????], ResNet [3], and use pretrained models to help
with the learning process.

\begin{figure}[bh]
\centering
\begin{subfigure}{0.4\textwidth}
\includegraphics[width=\textwidth]{Figures/ex_img1.png}
\centering
\end{subfigure}
\begin{subfigure}{0.4\textwidth}
\includegraphics[width=\textwidth]{Figures/ex_img2.png}
\centering
\end{subfigure}
\centering
\caption{Fluorescent Marks on Cell Samples}
\end{figure}

A csv file $train.csv$ contains all image ids and
their associated labels. The data is approximately $17 GB$ in size
(compressed) , and the training data is around $31K$ images.
Each image has a resolution of
$512 X 512$, which means that each image has $512^2$ features. Each
feature is an integer value between $[0, 255]$ .


\section*{Data Preparation}
We began by dividing our training data into three sets, split $80/10/10$,
selecting a random split that evenly distributed the labels. We used the
first set for training, the second for validation, and the final for test.
For the task of classification there are a few considerations. The first
being memory constraints. The dataset will not fit completely in memory,
so we deal with this issue in two ways. The first is that we write our
code so that we do not need the entire dataset to be in memory at the
same time during training or evaluation. We have written our
implementation in Pytorch [4], which will allow us to take advantage
of the Dataset object and manage our memory effectively. The second is
to downscale the resolution of the images, which will decrease the
dimensionality of a sample and allow us to fit more examples in memory
at runtime.

The second issue is with normalization. The images will be presented
to the model as a vector of $512^2$ values, and they are not centered
around $0$. Therefore we need to calculate the summary statistics that
will enable normalization. But since we are dealing with images, we
would have a large number of these statistics if we centered by features.
We decided to normalize by the channels instead which is done for
similar tasks [5]. We still have an issue with how to calculate the
statistics across the dataset, which we opted to instead sample $100$
images and compute the $8$ summary statistics for the $4$ channels. We
list our results below. It is important to note that we first divide
every pixel value by $255$ so that the range is $[0, 1]$ before we
calculate these statistics.

\begin{center}
\begin{tabular}{c | c | c | c | c}
&Red&Blue&Green&Yellow\\
\hline
mean&0.0764&0.0519&0.0502&0.0770\\
\hline
std&0.0197&0.0214&0.0110&0.0194\\
\end{tabular}
\end{center}

The third issue was that the images were not RGB but instead had $4$
channels. This is a problem because of the fourth issue, which is that
there is not enough data for models that we planned to use in this task.
While $31K$ is a large dataset objectively, deep learning models typically
require three times this amount of data to learn how to solves tasks from
scratch. If we have a small dataset then we will overfit and not
generalize well.
A solution to this, and the one we employ here, is to pretrain
the model on another dataset then fine tune it to task.

\section*{Exploratory Data Analysis}

The identification of certain proteins using fluorescent molecules is
represented by a green, red, blue, and yellow filter applied to each cell
image. Each filter distinguishes a particular area of interest in the cell.
As shown by the figure below, the
green shows the target protein structure of interest, blue shows nucleus,
red shows microtubules, and yellow shows endoplasmic reticulum [13].

\begin{figure}[bh]
\centering
\begin{subfigure}{0.7\textwidth}
\includegraphics[width=\textwidth]{Figures/filters.png}
\centering
\end{subfigure}
\centering
\caption{Image Filters Example [13]}
\end{figure}

We begin by exploring the summary statistics of the label counts for each
example, which we report below. We observe that most examples have either
one or two labels.

\begin{center}
\begin{tabular}{c|c|c|c|c|c|c}
Mean&Std&Min&25\%&Median&75\%&Max\\
\hline
1.63&0.70&1.0&1.0&2.0&2.0&5.0\\
\end{tabular}
\end{center}

When we examine the distribution of the $28$ labels in the training data,
we observe that it is very imbalanced. With the smallest class having
less than $16$ observations and the largest having over $10K$.
See Figure 11 for the full graph.

Next we investigate if there are correlations between certain variables.
We generate a heatmap of the labels in Figure 12. We observe that there
are not very strong correlations besides Lysosomes and Endosomes, and
Microtubules, Cytokinetic Bridge, and Mitotic Spindle. These correlations
make sense, because the process of cell degradation invovles the
interation between Lysosomes and Endosomes [6], and the latter are major
components of cytokinesis [7].

\section*{Data Management}
Due to the memory and processing constraints, we decided to implement
an AWS EC2 instance to help parallelize various calculations. We used
g3.4xlarge, which is equipped with a single Tesla M60 8GB.

Since the compressed data set was 17GB zipped, we were able to add 
an additional 200GB volume to avoid using the issue of the data being 
too large to hold in memory.  

\section*{Data Modeling}
\subsection*{Clustering}
One approach to try to localize features was to implement KMeans 
clustering. The first task to clustering was to decide how to organize 
the data in a way that could separate the cell's organelles. This proved
difficult due to the fact that each individual image was composed of a 
number of cells in a variety of locations and orientations. One Kaggle
user was able to use OpenCV to identify the nucleus of the cell in order
to create a visualization of the variety of cell orientations per image[14].

\begin{figure}[b]
\centering
\begin{subfigure}{.2\textwidth}
\includegraphics[width=\textwidth]{Figures/markers.png}
\centering
\end{subfigure}
\centering
\caption{Marked Cell Nuclei [14]}
\end{figure}

Assuming that the cells in each image could be recognized and counted,
and centroids were placed per cell the KMeans algorithm would create 
clusters based around the nuclei of cells rather than the internals of the
cell.

Another approach was to try to find the most distinguished cell in the 
image, extract it, then run KMeans clustering on that individual cell. The
issue with this approach is that the labels correspond to the image as a 
whole so if only one cell is looked at, then some features could be lost.

The solution that we tested was to cluster based on image
similarity with the idea that clustering may help distinguish labels.
By taking average adjusted rand index and silhouette scores 
over the labels we can determine whether or not that clustering 
method would be a good method to use to extract features. 
The first approach was to try to cluster based solely on the green
filter since this would give the proteins of interest with 512X512,
256X256, and 128X128 images.

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
Image Size & Avg. Adjusted Rand Index & Avg. Silhouette Score \\ \hline
512X512    & 0.027648                 & 0.196870              \\ \hline
256X256    & 0.017345                 & 0.544914              \\ \hline
128X128    & 0.007988                 & -0.074378             \\ \hline
\end{tabular}
\end{center}

Since the 256X256 images had the best silhouette score, this was used
as the image size for the remaining cluster calculations.
The next parameter that was tested was the number of clusters. 
These results are shown below. 

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
Number of Clusters & Avg. Adjusted Rand Index & Avg. Silhouette Score \\ \hline
2                  & 0.019196                 & 0.106003              \\ \hline
4                  & 0.0211310                & 0.120537              \\ \hline
8                  & 0.021007                 & 0.249482              \\ \hline
16                 & 0.015585                 & 0.3835657             \\ \hline
32                 & 0.0091347                & 0.6851891             \\ \hline
64                 & 0.00801588               & 0.7399999             \\ \hline
\end{tabular}
\end{center}

The silhouette score increased as the number of clusters increased. 
This makes sense since adding more clusters increases the distinct
 separation between each set of clusters. 

Hierarchial clustering was also used to determine if features could be
 extracted through clustering.  
The results of using hierarchial clustering are shown in the table below.

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
Clustering Techniques & Avg. Adjusted Rand Index & Avg. Silhouette Score \\ \hline
Hierarchial Clustering     & 0.011553              & 0.548045            \\ \hline
KMeans   & 0.014848                & 0.196014           \\ \hline
\end{tabular}
\end{center}

The results show that although hierarchial clustering gives a slightly improved
silhouette score, neither technique produces clusters that extract useful 
information about how to classify the labels. 

The last clustering method that was used was DBScan. This was used 
in contrast to KMeans since the number of initial clusters did not have 
to be specified. 

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
EPS & Avg. Adjusted Rand Index & Avg. Silhouette Score \\ \hline
.5     & 0.001654             & 0.576115            \\ \hline
2   & 0.001654              & 0.576115          \\ \hline
5   & 0.026029                & 0.3417283          \\ \hline
10   & 0.037471                & 0.479275          \\ \hline
\end{tabular}
\end{center}

We can see that by increasing the EPS the rand index score slightly increases.
However, the results are still showing that the clusters being formed are not
significant in determining labels. 

\subsection*{Classification}
We plan to model the relationship between the images and class labels
using a deep architecture, based of a well known architecture Resnet,
which overcomes many of the optimization challenges of these types of
models by batch normalization and residual learning [8, 9]. A
significant challenge is the relatively small dataset, which indicates
that fitting a model with such high complexity will lead to severe
overfitting. We mitigate this problem by using a transfer learning
method, with a pre-trained set of weights from a different but similar
task.

The common approach is to train on imagenet [10], but to use the models
trained on imagenet classification provided by pytorch [11] we modify the
first layer of the network so that it accepts $4$ channels as input
instead of the $3$ that it expects (because it expects RGB). We also need
to modify the fully connected layer at the end of the network, because
the pretrained model predicts $1$ of $1000$ classes. Below is a simplified
view of the prediction process, from image to a vector of scores.
\begin{center}
\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{Figures/arch.png}
\caption{Simplified View of Architecture}
\end{figure}
\end{center}

The architecture was inspired by models proposed in the kaggle forums [12].
Once we have the scores, which indicate the network's confidence in a
particular label, we need to decide what scores represent a positive label.
Using a similar approach to [12], we solve an optimization problem that
finds a set of thresholds which maximize a smoothed F1 score. Where
$\alpha$ is a tunable parameter. $p_i, \hat{p_i}$ represent the true and
predicted labels for the ith example.
\begin{equation*}
\begin{aligned}
&\underset{th}{\text{minimize}}
& & \sum_i (F1(p_i, \hat{p_i}) - 1)^2 + \alpha||\hat{p_i} - 0.5||^2\\
& & & \hat{p_i} = \frac{1}{1 +\exp(s_i-th)}\\
\end{aligned}
\end{equation*}

The results on using the focal loss, or the optimization were pretty
inconclusive. We suspect that there might be a slight error in our
implementation because others on kaggle were able to get significant
results. Below we list figures of our results. We used the standard
metrics, but calculated them two different ways. $micro$ means that
the score was calculated using true positives from every class, and
$macro$ was calculated by computing the metric for each class separately
and taking the average.

\newpage
\begin{center}
\begin{figure}
\centering
\begin{tabular}{c | c | c}
&CE&Focal Loss\\
\hline
F1&0.78&0.72\\
\hline
Precision&0.84&0.78\\
\hline
Recall&0.73&0.67\\
\end{tabular}
\caption{Metrics on Training Data using 0.5 Thresh MICRO}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\centering
\begin{tabular}{c | c | c}
&CE&Focal Loss\\
\hline
F1&0.52&0.47\\
\hline
Precision&0.75&0.69\\
\hline
Recall&0.46&0.40\\
\end{tabular}
\caption{Metrics on Training Data using 0.5 Thresh MACRO}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\centering
\begin{tabular}{c | c | c}
&CE&Focal Loss\\
\hline
F1&0.63&0.65\\
\hline
Precision&0.70&0.71\\
\hline
Recall&0.58&0.60\\
\end{tabular}
\caption{Metrics on Validation Data using 0.5 Thresh MICRO}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\centering
\begin{tabular}{c | c | c}
&CE&Focal Loss\\
\hline
F1&0.37&0.37\\
\hline
Precision&0.47&0.52\\
\hline
Recall&0.33&0.32\\
\end{tabular}
\caption{Metrics on Validation Data using 0.5 Thresh MACRO}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\centering
\begin{tabular}{c | c | c}
&CE&Focal Loss\\
\hline
F1&0.65&0.66\\
\hline
Precision&0.71&0.72\\
\hline
Recall&0.59&0.61\\
\end{tabular}
\caption{Metrics on Test Data using 0.5 Thresh MICRO}
\end{figure}
\end{center}

\begin{center}
\begin{figure}
\centering
\begin{tabular}{c | c | c}
&CE&Focal Loss\\
\hline
F1&0.37&0.37\\
\hline
Precision&0.47&0.52\\
\hline
Recall&0.33&0.32\\
\end{tabular}
\caption{Metrics on Test Data using 0.5 Thresh MACRO}
\end{figure}
\end{center}


\begin{center}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{Figures/label_counts.png}
\caption{Distribution of Labels (Log Scale)}
\end{figure}
\end{center}


\begin{center}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{Figures/heatmap.png}
\caption{Label Correlations}
\end{figure}
\end{center}

\newpage
\section*{References}
[1] https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1978277/

[2] https://www.kaggle.com/c/human-protein-atlas-image-classification

[3] https://arxiv.org/abs/1512.03385

[4] https://pytorch.org/

[5] https://github.com/pytorch/examples/blob/42e5b996718797e45c46a25c55b031e6768f8440/imagenet/main.py\#L89-L101

[6] https://micro.magnet.fsu.edu/cells/endosomes/endosomes.html

[7] https://micro.magnet.fsu.edu/cells/fluorescencemitosis/cytokinesissmall.html

[8] https://arxiv.org/pdf/1512.03385.pdf

[9] https://arxiv.org/pdf/1502.03167.pdf

[10] https://arxiv.org/pdf/1608.08614.pdf

[11] https://pytorch.org/docs/stable/torchvision/models.html

[12] https://www.kaggle.com/iafoss/pretrained-resnet34-with-rgby-0-460-public-lb

[13] https://www.kaggle.com/allunia/protein-atlas-exploration-and-baseline

[14] https://www.kaggle.com/jschnab/exploring-the-human-protein-atlas-images
\end{document}
