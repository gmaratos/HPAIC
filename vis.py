import os
import math
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

class Visualizer():
    """
    Visualizer Class. Contains all functions for visualizing the
    training data

    Arguments:
        dpath (string): path to the dataset
        inds (numpy array): contains the indicies of the training
            set
    """

    def __init__(self, dpath, inds):
        self.ds = pd.read_csv(os.path.join(dpath, 'train.csv'))
        self.label_names = {
            0:  "Nucleoplasm",
            1:  "Nuclear membrane",
            2:  "Nucleoli",
            3:  "Nucleoli fibrillar center" ,
            4:  "Nuclear speckles",
            5:  "Nuclear bodies",
            6:  "Endoplasmic reticulum",
            7:  "Golgi apparatus",
            8:  "Peroxisomes",
            9:  "Endosomes",
            10: "Lysosomes",
            11: "Intermediate filaments",
            12: "Actin filaments",
            13: "Focal adhesion sites",
            14: "Microtubules",
            15: "Microtubule ends",
            16: "Cytokinetic bridge",
            17: "Mitotic spindle",
            18: "M. organizing center",
            19: "Centrosome",
            20: "Lipid droplets",
            21: "Plasma membrane",
            22: "Cell junctions",
            23: "Mitochondria",
            24: "Aggresome",
            25: "Cytosol",
            26: "Cytoplasmic bodies",
            27: "Rods & rings"
        }
        self.inds = inds

    def summary_stats(self):
        lcounts = self.ds.iloc[self.inds]['Target']
        lcounts = lcounts.str.get_dummies(sep=' ')
        print(lcounts.sum(axis=1).describe())

    def show_label_counts(self):
        lcounts = self.ds.iloc[self.inds]['Target']
        lcounts = lcounts.str.get_dummies(sep=' ')
        lc = pd.DataFrame(lcounts.sum(axis=0)).reset_index()
        lc['Organelle'] = lc['index'].astype(int).map(self.label_names)
        lc['Counts'] = lc[0].map(lambda x:math.log(x)/math.log(2))
        lc = lc.sort_values('Counts')
        sns.barplot(y='Organelle', x='Counts', data=lc)
        plt.show()

    def show_label_corr(self):
        lcounts = self.ds.iloc[self.inds]['Target']
        lcounts = lcounts.str.get_dummies(sep=' ')
        lcounts = lcounts.rename(index=str, columns=self.label_names)
        lcounts = lcounts.rename(lambda x, self=self:\
            self.label_names[int(x)], axis='columns')
        lc = lcounts.sum(axis=1)
        lcounts = lcounts[lc > 1]
        sns.heatmap(lcounts.corr(), yticklabels=1, vmin=-1, vmax=1)
        plt.show()
