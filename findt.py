import numpy as np
from scipy.optimize import leastsq
from sklearn.metrics import precision_score, recall_score, f1_score,\
    confusion_matrix

class Thresh_Solver():
    """
    Thresh_Solver class. Calculate the best threshold for determining
    if an example belongs to a specific class

    Arguments:
        d (float): parameter used to saturate sigmoid
        wd (float): penalty for drifting too far from 0.5 during
            optimization
    """
    def __init__(self, d=1.0, wd=1.0):
        self.d = d
        self.wd = wd

    def sigmoid(self, x):
        return 1.0/(1.0 + np.exp(-x))

    def F1_soft(self, preds, targs, th=0.5):
        preds = self.sigmoid(self.d*(preds-th))
        targs = targs.astype(np.float)
        score = 2.0*(preds*targs).sum(axis=0)/((preds+targs).sum(axis=0) +\
            1e-6)
        return score

    def find_thresh(self, preds, true, show_diff=False):
        params = 0.5*np.ones(28)
        preds = self.sigmoid(preds)
        error = lambda p:np.concatenate(\
            (1.0 - self.F1_soft(preds, true, p), self.wd*(p - 0.5)),\
                axis=None)
        p, success = leastsq(error, params)
        if show_diff:
            print("Thresholds: ", p)
            print("F1 0.5: ", f1_score(true, preds > 0.5, average='micro'))
            print("F1: ", f1_score(true, preds > p, average='micro'))
        pred = preds > p
        cms = [confusion_matrix(true[:, i], pred[:, i]) for i in range(28)]
        pred1 = preds > 0.5
        cms1 = [confusion_matrix(true[:, i], pred1[:, i]) for i in range(28)]
        import pdb;pdb.set_trace()
        return p
