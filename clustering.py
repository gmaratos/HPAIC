import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
from sklearn.preprocessing import StandardScaler  
import glob
import pandas as pd
import scipy as sp
from scipy.cluster.hierarchy import linkage, fcluster
import sklearn
from sklearn import metrics
import seaborn as sns
from torch.utils.data import DataLoader
import random
from sklearn.cluster import KMeans
from torchvision.transforms import Normalize
from skimage.transform import resize
from AtlasDataset import AtlasDataset

X = AtlasDataset('../data', 'train')

imgs = []

np.random.seed(42)
data_len = len(X)
inds = random.sample(range(0,data_len),100)
data = []
labels = []

for i in inds:
    data.append(X[i][1])
    labels.append(X[i][1])
print(len(data))

data = pd.DataFrame(data)
labels = pd.DataFrame(labels)

#Find total of labels
sums = []
for i in range(len(labels.columns)):
    sums.append(labels[i].sum())
for val in sums:
    print(val)
scaler = StandardScaler()
scaler.fit(data, labels)
data_scaled = scaler.transform(data)

for j in [2,4,8,16,32,64]:
    clustering = sklearn.cluster.KMeans(n_clusters = j,init='random',random_state=0).fit(data_scaled)
    clusters = clustering.labels_
    ari_tot = 0
    sil_tot = 0
    print("Num clusters is:", j)
    for i in range(28):
        #print("Label num:", i)
        cont_matrix = sklearn.metrics.cluster.contingency_matrix(labels.iloc[:,i],clusters )
        #print(cont_matrix)
        adjusted_rand_index = metrics.adjusted_rand_score(labels.iloc[:,i], clusters)
        ari_tot = ari_tot + adjusted_rand_index
        silhouette_coefficient = metrics.silhouette_score(data_scaled, clusters)
        sil_tot = sil_tot + silhouette_coefficient
        #print([adjusted_rand_index, silhouette_coefficient])
    print("KMeans")
    print("Average ARI is:", ari_tot/28)
    print("Average SC is:", sil_tot/28)

clustering = sp.cluster.hierarchy.linkage(data_scaled,method='single',metric='euclidean')
clusters = fcluster(clustering, 28, criterion = 'maxclust')

ari_tot = 0
sil_tot = 0
for i in range(28):
    cont_matrix = sklearn.metrics.cluster.contingency_matrix(labels.iloc[:,i],clusters )
    print(cont_matrix)
    adjusted_rand_index = metrics.adjusted_rand_score(labels.iloc[:,i], clusters)
    silhouette_coefficient = metrics.silhouette_score(data_scaled, clusters)
    ari_tot = ari_tot + adjusted_rand_index
    sil_tot = sil_tot + silhouette_coefficient
    print([adjusted_rand_index, silhouette_coefficient])

print("Hierarchial Clustering")
print("Average ARI is:", ari_tot/28)
print("Average SC is:", sil_tot/28)

clustering = sklearn.cluster.DBSCAN(eps=2, min_samples=5).fit(data_scaled)
clusters = clustering.labels_
ari_tot = 0
sil_tot = 0
clustering = sklearn.cluster.DBSCAN(eps=10, min_samples=2).fit(data_scaled)
clusters = clustering.labels_
for i in range(28):
    cont_matrix = sklearn.metrics.cluster.contingency_matrix(labels.iloc[:,i],clusters )
    print(cont_matrix)
    adjusted_rand_index = metrics.adjusted_rand_score(labels.iloc[:,i], clusters)
    silhouette_coefficient = metrics.silhouette_score(data_scaled, clusters)
    ari_tot = ari_tot + adjusted_rand_index
    sil_tot = sil_tot + silhouette_coefficient
    print([adjusted_rand_index, silhouette_coefficient])
print("DBScan")
print("Average ARI is:", ari_tot/28)
print("Average SC is:", sil_tot/28)
