import argparse
import os
import torch
from AtlasDataset import AtlasDataset
from torch.utils.data import DataLoader
from net import Resnet34_mod
from loss import FocalLoss
from torch.nn import BCEWithLogitsLoss
from torch import optim
from findt import Thresh_Solver
from vis import Visualizer

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dpath', type=str, help='path to root folder')
    parser.add_argument('-g', help='use the gpu', action='store_true')
    parser.add_argument('-t', help='train net', action='store_true')
    parser.add_argument('-c', help='use BCE instead of Focal',\
        action='store_true')
    parser.add_argument('-v',\
        help='Enter data visualization and exploration mode',\
        action='store_true')
    parser.add_argument('--thres', type=str,\
        help='find thresholds using weights')
    args = parser.parse_args()

    if not os.path.isdir(args.dpath):
        print('Error: ', args.dpath, ' is not a directory')
        exit()
    if args.g:
        dev = torch.device('cuda:0')
    else:
        dev = torch.device('cpu')

    print('System State Initialized: ', args.dpath, ' ', dev)
    ds_train = AtlasDataset(args.dpath, 'train')
    ds_val = AtlasDataset(args.dpath, 'val')
    ds_test = AtlasDataset(args.dpath, 'test')
    dloader_train = DataLoader(ds_train, batch_size=100,\
        pin_memory=args.g, num_workers=5)
    dloader_val = DataLoader(ds_val, batch_size=100,\
        pin_memory=args.g, num_workers=5)
    dloader_test = DataLoader(ds_test, batch_size=100,\
        pin_memory=args.g, num_workers=5)

    if args.thres:
        if not os.path.exists(args.thres):
            print("No such model at path: ", args.thres)
            exit()
        model = Resnet34_mod().to(dev)
        model.load_state_dict(torch.load(args.thres))
        model.eval()
        print("\nCalculate Optimal Thresholds using: ", args.thres)
        tsolv = Thresh_Solver()
        pred_train, true_train = [], []
        for bind, batch in enumerate(dloader_train):
            inp = batch[0].to(dev)
            target = batch[1].to(dev)
            pred_train.append(model(inp).detach().cpu())
            true_train.append(target.cpu())
            print(" %d/%d ~ "%(bind+1, len(dloader_train)), end=' \r')
        print("")
        pred_train = torch.cat(pred_train, dim=0).numpy()
        true_train = torch.cat(true_train, dim=0).numpy()
        params = tsolv.find_thresh(pred_train, true_train, True)

    if args.v:
        print("Begin Visualization")
        vs = Visualizer(args.dpath, ds_train.inds)
        vs.summary_stats()
        vs.show_label_counts()
        vs.show_label_corr()

    if args.t:
        print("\nBegin Training")
        print("Loss Metric: ")
        if args.c:
            loss_fn = BCEWithLogitsLoss()
            print("Cross Entropy")
            loss_str = 'ce'
        else:
            loss_fn = FocalLoss()
            print("Focal Loss")
            loss_str = 'fl'
        model = Resnet34_mod().to(dev)
        opt = optim.Adam(model.parameters())

        for epoch in range(10):
            print("Epoch: ", epoch + 1)
            for bind, batch in enumerate(dloader_train):
                inp = batch[0].to(dev)
                target = batch[1].to(dev)
                pred = model(inp)
                loss = loss_fn(pred, target)
                opt.zero_grad()
                loss.backward()
                opt.step()
                print(" %d/%d ~ "%(bind+1, len(dloader_train)),\
                    loss, end=' \r')
            print("")
            with torch.no_grad():
                for bind, batch in enumerate(dloader_val):
                    inp = batch[0].to(dev)
                    target = batch[1].to(dev)
                    pred = model(inp)
                    loss = loss_fn(pred, target)
                    print(" %d/%d ~ "%(bind+1, len(dloader_val)),\
                        loss, end=' \r')
            print("")
        import pdb;pdb.set_trace()
        torch.save(model.state_dict(), 'model_weights_%s.pt'%loss_str)
